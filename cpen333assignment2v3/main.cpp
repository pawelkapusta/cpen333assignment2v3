// Names:		Pawel Kapusta	&	Edward Ngo
// Student IDs:	32181133		&	33826132

#include <stdio.h>		// for printf
#include <conio.h>		// for _kbhit(), getch() and getche()

#include "Car.h"
#include "Technician.h"
#include "Store.h"
#include "Garbage.h"
#include "Recycling.h"
#include "JobSheet.h"
#include "Customer.h"
#include "Receptionist.h"

void testSimulation()
{
	Store*		storeLocation = new Store();
	Garbage*	garbageLocation = new Garbage();
	Recycling*	recyclingLocation = new Recycling();

	Customer testCustomer("Bob", 12345, 131, 1, 10, 1, 10, 4, false);
	Technician testTechnician(storeLocation, garbageLocation, recyclingLocation);
	Receptionist testReceptionist(&testTechnician, &testCustomer);

	garbageLocation->PrintDisposedOilFilters();
	garbageLocation->PrintDisposedAirFilters();
	recyclingLocation->PrintRecycledOil();
	recyclingLocation->PrintRecycledTires();
	printf("\n");

	testReceptionist.PassVehicle();

	printf("\n");
	testReceptionist.getJobSheet()->printListOfItems();
	printf("\n");
	testReceptionist.getJobSheet()->printListOfLabourItems();
	printf("\n");

	garbageLocation->PrintDisposedOilFilters();
	garbageLocation->PrintDisposedAirFilters();
	recyclingLocation->PrintRecycledOil();
	recyclingLocation->PrintRecycledTires();
	printf("\n");

	testReceptionist.StampServiceRecord();
	testReceptionist.GenerateInvoice();
	testReceptionist.SendInvoice();
	double Payment = testCustomer.PayPayment();
	char* InvoiceDate = testCustomer.getInvoice()->getInvoiceDate();
}

int main()
{
	testSimulation();
	printf("\nSimulation Complete");
	getchar();
	return 1;
}