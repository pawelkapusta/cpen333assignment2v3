#pragma once

// Names:		Pawel Kapusta	&	Edward Ngo
// Student IDs:	32181133		&	33826132

#include <vector>

#include "OilFilter.h"
#include "AirFilter.h"

class Garbage
{

	std::vector<OilFilter*> GarbageOilFilters;
	std::vector<AirFilter*> GarbageAirFilters;

public:

	Garbage();
	~Garbage();

	void DepositGarbage(OilFilter* OldOilFilter) {
		GarbageOilFilters.push_back(OldOilFilter);
	}

	void DepositGarbage(AirFilter* OldAirFilter) {
		GarbageAirFilters.push_back(OldAirFilter);
	}

	void PrintDisposedOilFilters() {
		printf("Number of Disposed Oil Filters: %i \n", GarbageOilFilters.size());
	}

	void PrintDisposedAirFilters() {
		printf("Number of Disposed Air Filters: %i \n", GarbageAirFilters.size());
	}

};

Garbage::Garbage()
{
}

Garbage::~Garbage()
{
	for (std::vector<OilFilter*>::iterator it = GarbageOilFilters.begin(); it != GarbageOilFilters.end(); ++it)
	{
		delete (*it);
	}
	GarbageOilFilters.clear();

	for (std::vector<AirFilter*>::iterator it = GarbageAirFilters.begin(); it != GarbageAirFilters.end(); ++it)
	{
		delete (*it);
	}
	GarbageAirFilters.clear();
}