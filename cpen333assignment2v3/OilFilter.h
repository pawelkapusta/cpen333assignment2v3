#pragma once

// Names:		Pawel Kapusta	&	Edward Ngo
// Student IDs:	32181133		&	33826132

#include "CarParts.h" // To Inheret "Part Number" and "CostPerUnits" Properties

class OilFilter: public CarParts
{

public:

	OilFilter();
	~OilFilter();

};

OilFilter::OilFilter()
{
	// Default Part Number and Cost Properties
	setPartNumber(2);
	setCostPerUnit(30);
}

OilFilter::~OilFilter()
{
}