#pragma once

// Names:		Pawel Kapusta	&	Edward Ngo
// Student IDs:	32181133		&	33826132

#include <vector>

struct LabourItem
{
	int LabourType;
	int Cost;
	LabourItem(int _LabourType, int _Cost) : LabourType(_LabourType), Cost(_Cost) {}
};

struct Item 
{
	int PartNumber;
	int Quantity;
	int CostPerUnit;
	Item(int _PartNumber, int _Quantity, int _CostPerUnit) : PartNumber(_PartNumber), Quantity(_Quantity), CostPerUnit(_CostPerUnit) {}
};

class JobSheet
{

public:

	std::vector<LabourItem>	ListOfLabourItems;
	std::vector<Item>		ListOfItems;
	JobSheet();
	~JobSheet();

	void AddItem(int _PartNumber, int _Quantity, int _CostPerUnit) 
	{
		Item NewItem(_PartNumber, _Quantity, _CostPerUnit);
		ListOfItems.push_back(NewItem);
	}

	void AddLabourItem(int _LabourType, int _Cost) 
	{
		LabourItem NewLabourItem(_LabourType, _Cost);
		ListOfLabourItems.push_back(NewLabourItem);
	}

	void printListOfItems()
	{
		printf("Printing List of Items...\n");
		for (size_t i = 0; i < ListOfItems.size(); i++)
		{
			printf("Part Number: %i\n", ListOfItems[i].PartNumber);
			printf("Cost Per Unit: $%i.00\n", ListOfItems[i].CostPerUnit);
			printf("Quantity: %i\n", ListOfItems[i].Quantity);
		}
	}

	void printListOfLabourItems()
	{
		printf("Printing List of Labour Items...\n");
		for (size_t i = 0; i < ListOfLabourItems.size(); i++)
		{
			printf("Labour Type: %i\n", ListOfLabourItems[i].LabourType);
			printf("Cost: $%i.00\n", ListOfLabourItems[i].Cost);
		}
	}
};

JobSheet::JobSheet()
{
}

JobSheet::~JobSheet()
{
}
