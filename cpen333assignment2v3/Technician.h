#pragma once

// Names:		Pawel Kapusta	&	Edward Ngo
// Student IDs:	32181133		&	33826132

#include <stdio.h>
#include <algorithm>

#include "Car.h"
#include "JobSheet.h"
#include "Store.h"
#include "Garbage.h"
#include "Recycling.h"

#define SWAP_OIL_LABOUR_NUM		1
#define SWAP_OIL_LABOUR_COST	10

#define SWAP_OIL_FILT_LABOUR_NUM	2
#define SWAP_OIL_FILT_LABOUR_COST	20

#define SWAP_AIR_FILT_LABOUR_NUM	3
#define SWAP_AIR_FILT_LABOUR_COST	10

#define SWAP_TIRE_LABOUR_NUM	4
#define SWAP_TIRE_LABOUR_COST	40

#define ROT_TIRE_LABOUR_NUM		5
#define ROT_TIRE_LABOUR_COST	30

class Technician
{
	// Location of car to be serviced
	Car*		carToService;

	// Location of facilities to buy, dispose, and recycle parts
	Store*		store;
	Garbage*	garbage;
	Recycling*	recycling;

	// Pointers to hold new parts to install
	Oil*		newOil;
	OilFilter*	newOilFilter;
	AirFilter*	newAirFilter;
	Tires*		newTires;

	// Pointer on old car components
	Oil*		oldOil;
	OilFilter*	oldOilFilter;
	AirFilter*	oldAirFilter;
	Tires*		oldTires;

	// Job Sheet to record servicing transaction
	JobSheet*	jobSheet;

public:

	Technician(Store* _store, Garbage* _garbage, Recycling* _recycling);
	~Technician();

	// Service car
	JobSheet* service(Car* _carToService)
	{
		carToService = _carToService;
		jobSheet = new JobSheet;

		if (carToService == nullptr){ 
			printf("The technician is not assigned to a car.\n");
		}

		else {

			newOil = GetOil(carToService->getOil()->getType(), carToService->getOil()->getQuantity());
			AddItem(newOil, 1);

			newAirFilter = GetAirFilter();
			AddItem(newAirFilter, 1);

			newOilFilter = GetOilFilter();
			AddItem(newOilFilter, 1);

			oldOil = SwapOil(newOil);
			Recycle(oldOil);
			AddLabourItem(SWAP_OIL_LABOUR_NUM, SWAP_OIL_LABOUR_COST);

			oldOilFilter = SwapOilFilter(newOilFilter);
			Dispose(oldOilFilter);
			AddLabourItem(SWAP_OIL_FILT_LABOUR_NUM, SWAP_OIL_FILT_LABOUR_COST);

			oldAirFilter = SwapAirFilter(newAirFilter);
			Dispose(oldAirFilter);
			AddLabourItem(SWAP_AIR_FILT_LABOUR_NUM, SWAP_AIR_FILT_LABOUR_COST);

			CheckTiresForWear();

		}

		return jobSheet;
	}

private:

	// GetXXX retrieves parts from a never-ending store
	Oil* GetOil(int type, int quantity)
	{
		printf("Technician is getting %i L of Type %i Oil.\n", quantity, type);
		return store->BuyOil(type, quantity);
	}

	OilFilter* GetOilFilter()
	{
		printf("Technician is getting a new Oil Filter.\n");
		return store->BuyOilFilter();
	}

	AirFilter* GetAirFilter()
	{
		printf("Technician is getting a new Air Filter.\n");
		return store->BuyAirFilter();
	}

	Tires* GetTires(int make, int size, int quantity)
	{
		printf("Technician is getting %i new Make %i, Size %i Tire(s).\n", quantity, make, quantity);
		return store->BuyTires(make, size, quantity);
	}

	// Swaps pointer between part in the technician and car to effectively "swap" them in the simulation
	Oil* SwapOil(Oil* _newOil)
	{
		printf("Technician is swapping the oil.\n");
		Oil* temp = carToService->getOil();
		carToService->setOil(_newOil);
		return temp;
	}

	OilFilter* SwapOilFilter(OilFilter* _newOilFilter)
	{
		printf("Technician is swapping the oil filter.\n");
		OilFilter* temp = carToService->getOilFilter();
		carToService->setOilFilter(_newOilFilter);
		return temp;
	}

	AirFilter* SwapAirFilter(AirFilter* _newAirFilter)
	{
		printf("Technician is swapping the air filter.\n");
		AirFilter* temp = carToService->getAirFilter();
		carToService->setAirFilter(_newAirFilter);
		return temp;
	}

	Tires* SwapTires(Tires* _newTires)
	{
		printf("Technician is swapping the tires.\n");
		Tires* temp = carToService->getTires();
		carToService->setTires(_newTires);
		return temp;
	}

	void rotateTires()
	{
		printf("Technician is rotating the tires.\n");
		carToService->getTires()->toggleRotate();
	}

	void CheckTiresForWear()
	{
		if (carToService->getTires()->getWear() == true) {
			printf("Technician sees the tires are worn. \n");
			newTires = GetTires(carToService->getTires()->getMake(), carToService->getTires()->getSize(), carToService->getTires()->getQuantity());
			AddItem(newTires, carToService->getTires()->getQuantity());
			oldTires = SwapTires(newTires);
			Recycle(oldTires);
			AddLabourItem(SWAP_TIRE_LABOUR_NUM, SWAP_TIRE_LABOUR_COST);
		}
		else {
			printf("Technician sees the tires are not worn. \n");
			rotateTires();
			AddLabourItem(ROT_TIRE_LABOUR_NUM, ROT_TIRE_LABOUR_COST);
		}
	}

	// Dispose() is overloaded to take in 2 different class inputs
	void Dispose(AirFilter* _oldAirFilter)
	{
		printf("Technician is disposing of old air filter. \n");
		// Adds pointer of the part to dispose of to the garbage facility
		garbage->DepositGarbage(_oldAirFilter);
	}

	void Dispose(OilFilter* _oldOilFilter)
	{
		printf("Technician is disposing of old oil filter. \n");
		garbage->DepositGarbage(_oldOilFilter);
	}

	// Recycle behaves the same was as Dispose()
	void Recycle(Tires* _oldTires)
	{
		printf("Technician is recycling of old tires. \n");
		recycling->DepositRecycling(_oldTires);
	}

	// Recycle behaves the same was as Dispose()
	void Recycle(Oil* _oldOil)
	{
		printf("Technician is recycling of old oil. \n");
		recycling->DepositRecycling(_oldOil);
	}

	void AddItem(CarParts* carPart, int quantity)
	{
		jobSheet->AddItem(carPart->getPartNumber(), carPart->getCostPerUnit(), quantity);
	}

	void AddLabourItem(int labourType, int cost)
	{
		jobSheet->AddLabourItem(labourType, cost);
	}

};

// When creating technician, the technician needs to know the location of facilities to buy, dispose, and recycle parts
Technician::Technician(Store* _store, Garbage* _garbage, Recycling* _recycling)
	: store(_store), garbage(_garbage), recycling(_recycling)
{
}

Technician::~Technician()
{
	// Do not delete any pointer as it is the facilities that delete old items and the car deletes new items because they own them
}