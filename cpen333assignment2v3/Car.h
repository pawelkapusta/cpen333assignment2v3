#pragma once

// Names:		Pawel Kapusta	&	Edward Ngo
// Student IDs:	32181133		&	33826132

#include "Oil.h"
#include "OilFilter.h"
#include "AirFilter.h"
#include "Tires.h"

class Car
{

	Oil*		oil;
	OilFilter*	oilFilter;
	AirFilter*  airFilter;
	Tires*		tires;

public:

	Car(int oilType, int oilQuantity, int tireMake, int tireSize, int tireQuantity, bool tireWear);

	~Car();

	void setOil(Oil* newOil)					{ oil = newOil; }
	void setOilFilter(OilFilter* newOilFilter)	{ oilFilter = newOilFilter; }
	void setAirFilter(AirFilter* newAitFilter)	{ airFilter = newAitFilter; }
	void setTires(Tires* newTires)				{ tires = newTires; }

	Oil*		getOil()		{ return oil; }
	OilFilter*	getOilFilter()	{ return oilFilter; }
	AirFilter*	getAirFilter()	{ return airFilter; }
	Tires*		getTires()		{ return tires; }

private:

};

Car::Car(int oilType, int oilQuantity, int tireMake, int tireSize, int tireQuantity, bool tireWear)
{
	oil			= new Oil(oilType, oilQuantity);
	oilFilter	= new OilFilter();
	airFilter	= new AirFilter();
	tires		= new Tires(tireMake, tireSize, tireQuantity, tireWear);
}

Car::~Car()
{
	delete oil;
	delete oilFilter;
	delete airFilter;
	delete tires;
}