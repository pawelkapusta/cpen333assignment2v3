#pragma once

// Names:		Pawel Kapusta	&	Edward Ngo
// Student IDs:	32181133		&	33826132

class CarParts
{
	int PartNumber;
	int CostPerUnit;

public:

	CarParts();
	~CarParts();

	void setPartNumber(int _PartNumber)		{ PartNumber = _PartNumber; }
	void setCostPerUnit(int _CostPerUnit)	{ CostPerUnit = _CostPerUnit; }

	int getPartNumber()		{ return PartNumber; }
	int getCostPerUnit()	{ return CostPerUnit; }

};

CarParts::CarParts()
{
}

CarParts::~CarParts()
{
}