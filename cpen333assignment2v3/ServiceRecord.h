#pragma once

// Names:		Pawel Kapusta	&	Edward Ngo
// Student IDs:	32181133		&	33826132

class ServiceRecord
{

	char* DataOfLastService;

public:
	
	ServiceRecord();
	~ServiceRecord();

	void UpdateServiceDate(char* currDate) {
		DataOfLastService = currDate;
	}

private:

};

ServiceRecord::ServiceRecord() {};
ServiceRecord::~ServiceRecord() {};