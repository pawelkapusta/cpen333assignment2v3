#pragma once

// Names:		Pawel Kapusta	&	Edward Ngo
// Student IDs:	32181133		&	33826132

#include "CarParts.h" // To Inheret "Part Number" and "CostPerUnits" Properties

class Tires: public CarParts
{

	int make;
	int size;
	int quantity;
	bool wear;
	bool rotateToggle = false;

public:

	Tires(int _make, int _size, int _quantity, bool _wear);
	~Tires();

	int getMake()		{ return make; }
	int getSize()		{ return size; }
	int getQuantity()	{ return quantity; }
	bool getWear()		{ return wear; }
	void toggleRotate()	{ rotateToggle = !rotateToggle; }
};

Tires::Tires(int _make, int _size, int _quantity, bool _wear) :
	make(_make), size(_size), quantity(_quantity), wear(_wear)
{
	// Default Part Number and Cost Properties
	setPartNumber(4);
	setCostPerUnit(40);
}

Tires::~Tires()
{
}