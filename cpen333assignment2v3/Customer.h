#pragma once

// Names:		Pawel Kapusta	&	Edward Ngo
// Student IDs:	32181133		&	33826132

#include "Car.h"
#include "ServiceRecord.h"
#include "Invoice.h"
#include <list>
#include <string>

class Customer
{

	ServiceRecord *serviceRecord;
	Car* car;
	int customerID;
	double money;
	std::string name;
	Invoice *invoice;

public:


	Customer(std::string _name, int _customerID, double _money, int _oilType, int _oilQuantity, int _tireMake, int _tireSize, int _tireQuantity, bool _tireWear);
	~Customer();

	Car* SendCar() { return car; }
	std::string getName() { return name; }
	Invoice* getInvoice() { return invoice; }
	void setInvoice(Invoice* _invoice) { invoice = _invoice; }

	ServiceRecord *SendServiceRecord() { return serviceRecord; };

	void ComeBackLater() {
		// For Future Use
	}

	double PayPayment() {

		if (invoice->getInvoiceCost() <= money)
		{
			printf("Customer paid invoice of: $%0.2lf\n", invoice->getInvoiceCost());
			money -= invoice->getInvoiceCost();
			return invoice->getInvoiceCost();
		}
		else
		{
			printf("Oh No! The customer cannot afford the invoice and ran away\n");
			return 0;
		}
	}

private:

};

Customer::Customer(std::string _name, int _customerID, double _money, int _oilType, int _oilQuantity, int _tireMake, int _tireSize, int _tireQuantity, bool _tireWear)
{
	name = _name;
	customerID = _customerID;
	money = _money;
	car = new Car(_oilType, _oilQuantity, _tireMake, _tireSize, _tireQuantity, _tireWear);
	serviceRecord = new ServiceRecord();
	printf("%s has requested service on his vehicle\n\n", name.c_str());
}

Customer::~Customer()
{
	delete serviceRecord;
	delete car;
}