#pragma once

// Names:		Pawel Kapusta	&	Edward Ngo
// Student IDs:	32181133		&	33826132

#include "CarParts.h" // To Inheret "Part Number" and "CostPerUnits" Properties

class Oil : public CarParts
{
	int type;
	int quantity;

public:

	Oil(int _type, int _quantity);
	~Oil();

	int getType() { return type; }
	int getQuantity() { return quantity; }

};

Oil::Oil(int _type, int _quantity) : type(_type), quantity(_quantity)
{
	// Default Part Number and Cost Properties
	setPartNumber(1);
	setCostPerUnit(10);
}

Oil::~Oil()
{
}