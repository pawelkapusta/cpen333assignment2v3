#pragma once

// Names:		Pawel Kapusta	&	Edward Ngo
// Student IDs:	32181133		&	33826132

#include "Oil.h"
#include "OilFilter.h"
#include "AirFilter.h"
#include "Tires.h"

class Store
{

public:

	Store();
	~Store();

	Oil* BuyOil(int type, int quantity) {
		return new Oil(type, quantity);
	}

	OilFilter* BuyOilFilter() {
		return new OilFilter();
	}

	AirFilter* BuyAirFilter() {
		return new AirFilter();
	}

	Tires* BuyTires(int make, int size, int quantity) {
		return new Tires(make, size, quantity, false);
	}

};

Store::Store()
{
}

Store::~Store()
{
}