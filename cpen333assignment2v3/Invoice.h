#pragma once

// Names:		Pawel Kapusta	&	Edward Ngo
// Student IDs:	32181133		&	33826132

class Invoice
{
	char* Date;
	double TotalCost;

public:

	Invoice();
	~Invoice();

	char* getInvoiceDate() { return Date; }
	double getInvoiceCost() { return TotalCost; }

	void UpdateInvoice(char* currDate, double cost) {
		Date = currDate;
		TotalCost = cost;
	}

private:

};

Invoice::Invoice() {};

Invoice::~Invoice() {
};