#pragma once

// Names:		Pawel Kapusta	&	Edward Ngo
// Student IDs:	32181133		&	33826132

#include <vector>

#include "Oil.h"
#include "Tires.h"

class Recycling
{

	std::vector<Oil*>	RecycledOil;
	std::vector<Tires*> RecycledTires;

public:
	Recycling();
	~Recycling();

	void DepositRecycling(Oil* OldOil) {
		RecycledOil.push_back(OldOil);
	}

	void DepositRecycling(Tires* OldTires) {
		RecycledTires.push_back(OldTires);
	}

	void PrintRecycledOil() {
		printf("Number of Recycled Oil: %i \n", RecycledOil.size());
	}

	void PrintRecycledTires() {
		printf("Number of Recycled Tire Sets: %i \n", RecycledTires.size());
	}

private:

};

Recycling::Recycling()
{
}

Recycling::~Recycling()
{
	for (std::vector<Oil*>::iterator it = RecycledOil.begin(); it != RecycledOil.end(); ++it)
	{
		delete (*it);
	}
	RecycledOil.clear();

	for (std::vector<Tires*>::iterator it = RecycledTires.begin(); it != RecycledTires.end(); ++it)
	{
		delete (*it);
	}
	RecycledTires.clear();
}