#pragma warning(disable:4996)
#pragma once

// Names:		Pawel Kapusta	&	Edward Ngo
// Student IDs:	32181133		&	33826132

#include <stdio.h>
#include "Car.h"
#include "Customer.h"
#include "Technician.h"
#include "JobSheet.h"
#include "ServiceRecord.h"
#include "Invoice.h"
#include <ctime>

class Receptionist
{
	Customer *customer;
	Technician *technican;
	
	ServiceRecord * serviceRecord;
	Invoice *invoice;
	JobSheet * jobSheet;

public:

	Receptionist(Technician *_technican, Customer *_customer);
	~Receptionist();
	
	void PassVehicle() {
		printf("Receptionist handed %s's car over to the technician\n\n", (customer->getName()).c_str());
		Car *CustomerVehicle = customer->SendCar();
		jobSheet = technican->service(CustomerVehicle);
	}
	
	void MakeCoffee() {};
	
	void GenerateInvoice() {
		double cost = 0;

		for (size_t i = 0; i < jobSheet->ListOfLabourItems.size(); i++) {
			cost += jobSheet->ListOfLabourItems[i].Cost;
		}

		for (size_t i = 0; i < jobSheet->ListOfItems.size(); i++) {
			int Quantity = jobSheet->ListOfItems[i].Quantity;
			double PartCost = jobSheet->ListOfItems[i].CostPerUnit;

			cost += PartCost * Quantity;
		}

		time_t     now = time(0);
		struct tm  tstruct;
		char       currTime[80];
		tstruct = *localtime(&now);
		strftime(currTime, sizeof(currTime), "%Y-%m-%d %X", &tstruct);

		invoice->UpdateInvoice(currTime, cost);
		printf("Receptionist generated invoice on: %s\n", invoice->getInvoiceDate());
		printf("Invoice amount: $%0.2lf\n\n", invoice->getInvoiceCost());

	}

	void SendInvoice() {
		customer->setInvoice(invoice);
	}

	JobSheet* getJobSheet() { return jobSheet; }

	void StampServiceRecord() {
		serviceRecord = customer->SendServiceRecord();
		
		time_t     now = time(0);
		struct tm  tstruct;
		char       currTime[80];
		tstruct = *localtime(&now);
		strftime(currTime, sizeof(currTime), "%Y-%m-%d %X", &tstruct);
		printf("Receptionist stamped service record on: %s\n", currTime);
		serviceRecord->UpdateServiceDate(currTime);
	}

private:
	
};

Receptionist::Receptionist(Technician *_technican, Customer *_customer) {
	technican = _technican;
	customer = _customer;
	invoice = new Invoice();
};
Receptionist::~Receptionist() {
	delete invoice;
};