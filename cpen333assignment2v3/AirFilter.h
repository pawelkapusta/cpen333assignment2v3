#pragma once

// Names:		Pawel Kapusta	&	Edward Ngo
// Student IDs:	32181133		&	33826132

#include "CarParts.h" // To Inheret "Part Number" and "CostPerUnits" Properties

class AirFilter : public CarParts
{

public:

	AirFilter();
	~AirFilter();

};

AirFilter::AirFilter()
{
	// Default Part Number and Cost Properties
	setPartNumber(3);
	setCostPerUnit(20);
}

AirFilter::~AirFilter()
{
}